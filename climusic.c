/* climusic.c
 *  A command line music player made with
 *  ncurses and the mpv C library.
 *  Author: Lucas Santos
 *
 *  02/05/2022
 */

#define _XOPEN_SOURCE 600

/* standard glibc */
#include <stdio.h>
#include <locale.h>
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <ftw.h>
#include <pthread.h>

/* additional features */
#include <linux/limits.h>
#include <mpv/client.h>
#include <ncurses.h>

/* constants for colors */
#define TITLE_COLORS 1
#define NORMAL_COLORS 2

/* configuration TODO: make it user configurable via file */
#define INIT_VOLUME 50 // in %
#define MAX_VOLUME 100 // in %
#define SHUFFLE 1 // boolean
#define DEBUG 1 // boolean
#define LOOP_PLAYLIST 1 // boolean
#define MAX_FILES 256
#define FILE_TYPES {"mp3", "flac", "ogg", "m4a", "wav"}
#define FILE_TYPES_COUNT 5

/* global variables */
mpv_handle *ctx;
int numrow, numcol;
int file_count;
int milis_time = 0;
char file_list[MAX_FILES][PATH_MAX];
bool has_ended = false;
char file_types[FILE_TYPES_COUNT][16] = FILE_TYPES;

/* function prototypes */
void end_program(const char msg[]);
void format_time(int64_t time, int64_t endpos, char *buff);
static inline void check_error(int status);
void play(char filename[]);
void clrln(int row);
void print_middle(int row, char text[]);
int wcstlen(wchar_t **s, const int len);
void wprint_middle(int row, char text[]);
int store_file(const char *fpath, const struct stat *sb, int typeflag);
void *mpv_event_handler(void *data);
void *milis_timer(void *data);
void shuffle_playlist();


int main(int argc, char *argv[]) {

    if (argc == 1) { // exit if no argument is received
        printf("Please enter a folder in the first argument.\n");
        exit(1);
    }

    /* integers */
    int key;
    int volume = INIT_VOLUME;
    int64_t endpos;
    int pos_offset;
    int pause_time;

    /* booleans */
    bool hascolors = false;
    bool muted = false;
    bool shuffle = SHUFFLE;
    bool paused = false;
    bool quit = false;
    bool loop_current = false;

    /* buffers */
    char buff[128];

    /* create and initialize mpv */
    ctx = mpv_create();
    if (!ctx) {
        printf("mpv: failed creating context!\n");
        exit(1);
    }
    /* set mpv options */
    check_error(mpv_set_option_string(ctx, "audio-display", "no"));
    check_error(mpv_set_option_string(ctx, "af", "loudnorm=I=-15"));
    check_error(mpv_initialize(ctx));
    sprintf(buff, "%d", INIT_VOLUME);
    check_error(mpv_set_option_string(ctx, "volume", buff));
    check_error(mpv_set_property(ctx, "shuffle", MPV_FORMAT_FLAG, &shuffle));


    /* ncurses initialization */
    setlocale(LC_ALL, ""); // change locale temporarily to initalize curses screen
    initscr(); // initialize screen
    raw(); // disables line buffering
    noecho(); // disables echoing of user input
    curs_set(0); // hide cursor
    keypad(stdscr, TRUE); // enable reading of function keys, arrow keys and more
    getmaxyx(stdscr, numrow, numcol); // get screen size
    timeout(500); // getch() timeout
    hascolors = has_colors(); // check for colors
    setlocale(LC_NUMERIC, "C"); // go back to C locale

    /* color configuration */
    if (hascolors) {
        start_color();
        /* initialize color pairs */
        init_pair(TITLE_COLORS, COLOR_YELLOW, COLOR_BLACK);
        init_pair(NORMAL_COLORS, COLOR_WHITE, COLOR_BLACK);

        /* print title */
        attron(COLOR_PAIR(TITLE_COLORS)); // title text colors
        attron(A_BOLD); // enables bold printing
        print_middle(0, "[ =CLIMUSIC= ]"); // print title
        attroff(A_BOLD); // disables bold printing
        attron(COLOR_PAIR(NORMAL_COLORS)); // returns to default text

    } else {
        print_middle(0, "[ =CLIMUSIC= ]");
    }

    /* print basic structure */
    print_middle(1, "Reading...");
    print_middle(2, "00:00:00 / 00:00:00");
    sprintf(buff, "Volume: %d/%d", INIT_VOLUME, MAX_VOLUME);
    print_middle(3, buff);
    print_middle(4, "Unmuted ");
    print_middle(5, "Not looping current file");
    print_middle(10, "Press 'q' to exit...");
    refresh(); // refresh curses screen

    ftw(argv[1], store_file, 12); // read file list
    shuffle_playlist();

    /* code to play a track and run the interface */
    pthread_t thread_timer;

    pthread_create(&thread_timer, NULL, milis_timer, NULL); // start timer thread


    do { // loop playlist
        int i = 0;
        do { // play playlist
            if (quit) break;

            play(file_list[i]); // play track

            /* set values to start timing */
            pos_offset = milis_time;

            /* get track metadata */
            check_error(mpv_get_property(ctx, "duration", MPV_FORMAT_INT64, &endpos)); // get duration
            strcpy(buff, mpv_get_property_string(ctx, "media-title")); // get track name

            /* print music name */
            wprint_middle(1, buff);

            /* print initial time */
            format_time((milis_time-pos_offset)/1000, endpos, buff);
            print_middle(2, buff);

            refresh();

            /* loop while playing the music */
            while (!quit && (endpos*1000 >= milis_time-pos_offset)) {

                /* Get current music position and show it */
                if (!paused) {
                    format_time((milis_time-pos_offset)/1000, endpos, buff);
                    print_middle(2, buff);
                }

                key = getch();

                if (key != -1 && DEBUG) {
                    mvprintw(18, 0, "%d\n", key);
                    refresh();
                }

                /* interpret keys */
                switch (key) {
                    case 113: // letter 'q'
                        /* exit the program */
                        quit = true;
                        break;
                    case 3: // CTRL-C
                        /* exit the program */
                        quit = true;
                        break;
                    case 109: // letter 'm'
                        /* Toggle mute */
                        if (muted) {
                            sprintf(buff, "%d", volume);
                            check_error(mpv_set_option_string(ctx, "volume", buff));
                            print_middle(4, "Unmuted ");
                            muted = false;
                        } else {
                            check_error(mpv_set_option_string(ctx, "volume", "0"));
                            print_middle(4, "Muted ");
                            muted = true;
                        }
                        refresh();
                        break;
                    case 91: // character '['
                        /* Increase volume by 5 */
                        if (volume <= MAX_VOLUME-5) { // don't let sound above limit
                            volume += 5;
                            if (!muted) {
                                sprintf(buff, "%d", volume);
                                check_error(mpv_set_option_string(ctx, "volume", buff));
                            }
                            sprintf(buff, "Volume: %d/%d", volume, MAX_VOLUME);
                            print_middle(3, buff);
                            refresh();
                        }
                        break;
                    case 93: // character ']'
                        /* Decrease volume by 5 */
                        if (volume >= 5) { // don't let sound go negative
                            volume -= 5;
                            if (!muted) {
                                sprintf(buff, "%d", volume);
                                check_error(mpv_set_option_string(ctx, "volume", buff));
                            }
                            sprintf(buff, "Volume: %d/%d", volume, MAX_VOLUME);
                            print_middle(3, buff);
                            refresh();
                        }
                        break;
                    case KEY_RIGHT:
                        /* goes ahead 5 seconds */
                        mpv_command_string(ctx, "seek 5");
                        pos_offset -= 5000;
                        format_time((milis_time-pos_offset)/1000, endpos, buff);
                        print_middle(2, buff);
                        break;
                    case KEY_LEFT:
                        /* goes back 5 seconds */
                        mpv_command_string(ctx, "seek -5");
                        if ((milis_time-pos_offset)-5000 < 0) pos_offset = milis_time;
                        else pos_offset += 5000;
                        format_time((milis_time-pos_offset)/1000, endpos, buff);
                        print_middle(2, buff);
                        break;
                    case KEY_UP:
                        /* goes ahead 60 seconds */
                        mpv_command_string(ctx, "seek 60");
                        pos_offset -= 60000;
                        format_time((milis_time-pos_offset)/1000, endpos, buff);
                        print_middle(2, buff);
                        break;
                    case KEY_DOWN:
                        /* goes back 60 seconds */
                        mpv_command_string(ctx, "seek -60");
                        if ((milis_time-pos_offset)-60000 < 0) pos_offset = milis_time;
                        else pos_offset += 60000;
                        format_time((milis_time-pos_offset)/1000, endpos, buff);
                        print_middle(2, buff);
                        break;
                    case 10: // enter key
                        /* finishes current music */
                        pos_offset = -((milis_time-pos_offset) + endpos*1000);
                        pause_time = milis_time; // reset pause timer
                        refresh();
                        break;
                    case 32: // spacebar
                        /* pauses/unpauses music */
                        if (paused) {
                            print_middle(5, "Unpaused");
                            paused = false;
                            pos_offset += milis_time-pause_time;
                            check_error(mpv_command_string(ctx, "cycle pause"));
                        } else {
                            print_middle(5, "Paused");
                            paused = true;
                            pause_time = milis_time;
                            check_error(mpv_command_string(ctx, "cycle pause"));
                        }
                        refresh();
                        break;
                    case 108:
                        /* toggles music looping */
                        if (!loop_current) {
                            loop_current = true;
                            print_middle(5, "looping current file");
                        }
                        else {
                            loop_current = false;
                            print_middle(5, "Not looping current file");
                        }
                        break;
                } // end switch case
            } // end getch() loop
            if (!loop_current) i++; // loop current music logic
        } while (i < file_count); // end music
        if (quit) break;
    } while (LOOP_PLAYLIST); // end playlist loop

    end_program("Program terminated successfully!");

}

/* function to exit out of the program correctly */
void end_program(const char msg[]) {
    endwin();
    mpv_terminate_destroy(ctx);
    printf("Exit message: %s\n", msg);
    exit(1);
}

void format_time(int64_t time, int64_t endpos, char *buff) {
    int hour = time / 3600;
    int min = (time % 3600)/60;
    int sec = time % 60;

    int endhour = endpos / 3600;
    int endmin = (endpos % 3600)/60;
    int endsec = endpos % 60;

    sprintf(buff, "%02d:%02d:%02d / %02d:%02d:%02d",
            hour, min, sec, endhour, endmin, endsec);
}

/* mpv function for error checking */
static inline void check_error(int status) {
    if (status < 0) {
        end_program(mpv_error_string(status));
    }
}

/* play a file */
void play(char filename[]) {
    mpv_event *event;

    const char *cmd[] = {"loadfile", filename, NULL}; // command to run in mpv
    mpv_command(ctx, cmd); // play audio
    /* wait for media to load */
    do {
        event = mpv_wait_event(ctx, -1); // waits for mpv event
    } while (event->event_id != MPV_EVENT_FILE_LOADED);

}

void clrln(int row) {
    move(row, 0); // move to beggining of line
    clrtoeol(); // clear line
}

/* print on the middle of the terminal (simple) */
void print_middle(int row, char text[]) {
    int width = strlen(text);
    clrln(row);
    mvaddstr(row, numcol/2-width/2, text);
}

/* wide character string length (on terminal) */
int wcstlen(wchar_t **s, const int len) {
    setlocale(LC_ALL, "");
    wchar_t *ptr = *s;
    int count = 0;
    for (int i = 0; i < len; i++) {
        if (*ptr == L'\0') break;
        /* Source:
         * https://www.localizingjapan.com/blog/2012/01/20/regular-expressions-for-japanese-text/
         *
         * this conditional checks for some wide character ranges
         */

        if ((*ptr >= L'\x3041' && *ptr <= L'\x3096') || // hiragana
            (*ptr >= L'\x30A0' && *ptr <= L'\x30FF') || // katakana (Full Width)
            /* kanji */
            (*ptr >= L'\x3400' && *ptr <= L'\x4DB5') ||
            (*ptr >= L'\x4E00' && *ptr <= L'\x9FCB') ||
            (*ptr >= L'\xF900' && *ptr <= L'\xFA6A') ||
            (*ptr >= L'\x2E80' && *ptr <= L'\x2FD5') || // kanji radicals
            (*ptr >= L'\xFF5F' && *ptr <= L'\xFF9F') || // japanese symbols and punctuation
            (*ptr >= L'\xFF01' && *ptr <= L'\xFF5E') || // alphanumeric and punctuation
            /* miscellaneous japanese symbols and characters */
            (*ptr >= L'\x31F0' && *ptr <= L'\x31FF') ||
            (*ptr >= L'\x3220' && *ptr <= L'\x3243') ||
            (*ptr >= L'\x3280' && *ptr <= L'\x337F') ||
            /* individual characters */
            (*ptr == L'〔' || *ptr == L'〕') ||
            (*ptr == L'【' || *ptr == L'】')
        )
            count += 2; // count these characters as 2 (because of their width)
        else
            count++;
        ptr++; // go to the next letter
    }
    setlocale(LC_NUMERIC, "C");
    return count;
}

/* print on the middle of the terminal (complex) */
void wprint_middle(int row, char text[]) {
    setlocale(LC_ALL, ""); // change locale temporarily
    int len = strlen(text);
    wchar_t *buff = (wchar_t*) calloc(len+1, sizeof(wchar_t)); // should be enough memory
    int width = mbstowcs(buff, text, len);
    int twidth = wcstlen(&buff, width); // get terminal width

    clrln(row); // clear line
    mvaddstr(row, numcol/2-twidth/2, text);
    free(buff);
    setlocale(LC_NUMERIC, "C"); // go back to C locale
}

/* function called with nftw to read file paths */
int store_file(const char *fpath, const struct stat *sb, int typeflag) {
    if (typeflag == FTW_F) {
        int len;

        len = strlen(fpath); // start from second last leter (ignore last)
        for (int i = len-2; i > 0; i--) {
            if (fpath[i] == '.') {
                char *buff = calloc(len-i, sizeof(char));
                char ch[2];

                for (int j = i+1; j < len; j++) {
                    ch[0] = fpath[j];
                    ch[1] = '\0';
                    strcat(buff, ch);
                }
                for (int k = 0; k < FILE_TYPES_COUNT; k++) {
                    if (!strcmp(buff, file_types[k])) {
                        strcpy(file_list[file_count], fpath);
                        file_count++;
                    }
                }
                break;
            }
        }
    }

    return 0;
}

void *mpv_event_handler(void *data) {
    mpv_event *event;

    while (1) {
        event = mpv_wait_event(ctx, 100);
        if (event->event_id == MPV_EVENT_END_FILE)
            has_ended = true;
    }
}

void *milis_timer(void *data) {
    /* 100ms precision */
    while (1) {
        usleep(100000);
        milis_time += 100;
    }
}

void shuffle_playlist() {
    char buffer[PATH_MAX];
    int rnum;

    for (int i = 0; i < file_count; i++) {
        rnum = rand()%file_count;
        strcpy(buffer, file_list[i]);
        strcpy(file_list[i], file_list[rnum]);
        strcpy(file_list[rnum], buffer);
    }
}
