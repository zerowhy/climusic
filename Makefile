climusic:	.FORCE
	gcc -Wall -o climusic climusic.c -lmpv -lncursesw

.FORCE:

install:
	cp climusic ~/.local/bin/climusic

test:	climusic
	./climusic ./test_playlist
