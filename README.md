# THIS PROJECT HAS BEEN ARCHIVED AND IS NO LONGER IN DEVELOPMENT
After trying to fix some of the most annoying bugs on this program, I quickly realized I had to do a full rewrite of major parts of the code to fix the bugs.\
With that in mind, I rewrote the entire application! Here it is: [tuneclip](https://gitlab.com/zerowhy/tuneclip)!


Original README:

# [CLIMusic](https://gitlab.com/zerowhy/climusic)

Welcome to the CLIMusic repository!

CLIMusic is a command-line application to play music directly through the terminal without any graphical window.

![](https://gitlab.com/zerowhy/climusic/-/raw/master/climusic.gif)

## Functionality

This application currently only plays music by passing in a folder in the first argument of the program; see syntax below:
```
climusic <path_of_folder> 
```

After the program starts playing the folder, there are some controls available to the user:

| Key         | Action              |
|-------------|---------------------|
| Spacebar    | Play/Pause          |
| m           | Mute Sound          |
| [           | Volume up           |
| ]           | Volume Down         |
| Arrow up    | Go forward 60s      |
| Arrow down  | Go backwards 60s    |
| Arrow right | Go forward 5s       |
| Arrow left  | Go backwards 5s     |
| Enter       | Next track          |
| l           | Loop current music  |
| q/CTRL-C    | Quit application    |

These controls are currently not user-configurable, but that is a planned feature.
If you experience any bugs, please report them in the [GitLab Issues](https://gitlab.com/zerowhy/climusic/-/issues).

## Compiling

In order to compile CLIMusic, you will have to install some dependencies:
- mpv library
- ncurses library

With the correct dependencies installed, run `make` to compile. This command will generate a executable: `climusic`

Currently running `make install` will not work, as it is only used for internal testing and should **not** be used in any other system. If you feel creative, you can try editing the Makefile to install the app on a folder of your choice!
